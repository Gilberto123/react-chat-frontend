import './App.css';
import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { UserContext } from './UserContext';
import Chat from './components/chat/Chat';
import Home from './components/home/Home';
import Navbar from './components/Layout/Navbar';

function App() {
  const [user, setUser] = useState(null);
  return (
    <Router>
      <div className="App">
        <UserContext.Provider value={{ user, setUser }}>
          <Navbar></Navbar>
          <Switch>
            <Route path='/chat' component={Chat}></Route>
            <Route path='/' component={Home}></Route>
          </Switch>
        </UserContext.Provider>
      </div>
    </Router>
  );
}

export default App;
