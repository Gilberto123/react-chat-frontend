import React from 'react'

const RoomItemList = ({ room }) => {
    return (
        <div className="card horizontal">
            <div className="card-stacked">
                <div className="card-content">
                    <p>{room}</p>
                </div>
            </div>
        </div>

    )
}

export default RoomItemList
