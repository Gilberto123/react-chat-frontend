import React from 'react'
import RoomItem from './RoomItemList';

const RoomList = ({rooms}) => {
    return (
        <div>
            {rooms && rooms.map( room => (
                <RoomItem key={room._id} room={room.name}></RoomItem>
            ))}
        </div>
    )
}

export default RoomList
