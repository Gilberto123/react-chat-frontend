import React from 'react';
import { Link } from 'react-router-dom';

const Navbar = () => {
    return (
        <>
            <nav>
                <div className="nav-wrapper">
                    <Link to='/' className="brand-logo">Chat</Link>
                    <Link to='' data-target="mobile-demo" className="sidenav-trigger"><i className="material-icons">menu</i></Link>
                    <ul className="right hide-on-med-and-down">
                        <li><a href="sass.html">LogIn</a></li>
                        <li><a href="badges.html">SignIn</a></li>
                        <li><a href="collapsible.html">LogOut</a></li>
                    </ul>
                </div>
            </nav>

            <ul className="sidenav" id="mobile-demo">
                <li><a href="sass.html">LogIn</a></li>
                <li><a href="badges.html">SignIn</a></li>
                <li><a href="collapsible.html">LogOut</a></li>
            </ul>
        </>
    )
}

export default Navbar
